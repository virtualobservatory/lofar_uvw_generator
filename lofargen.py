import numpy
from argparse import ArgumentParser
import json
import logging
from lofardatagenerator.coords import *

logger = logging.getLogger('LOFAR generator')
logger.setLevel(logging.DEBUG)

__MINUTE_IN_SECONDS = 60

def parse_arguments():
    parser = ArgumentParser('Generate uvw and compute eccentricity and '
                            'filling factor of the uvw plane')
    parser.add_argument('array_coordinates',
                        type=str,
                        help='coordinate list of the array elements')

    parser.add_argument('observation_duration',
                        type=int,
                        help='duration of the observation in minutes')
    parser.add_argument('--n_antennas',
                        type=int,
                        default=-1,
                        help='antenna sample size')

    parser.add_argument('--source_coordinates',
                        type=str,
                        default='13:20:01.48 +45:00:00.80',
                        help='source_coordinates')
    parser.add_argument('--make_uvw_plot', action='store_true')
    parser.add_argument('--random_seed',
                        default=numpy.random.seed(),
                        type=int,
                        help='seed for the random number generator')

    parser.add_argument('--frequency',
                        default=150.,
                        type=float,
                        help='frequency in MHz')

    parser.add_argument('--max_baseline_length',
                        default=-1,
                        type=float,
                        help='max baseline length in m')
    parser.add_argument('--save_image_to', type=str, default='.')
    return parser.parse_args()



def main():
    start_time = datetime.now()
    arguments = parse_arguments()
    numpy.random.seed(arguments.random_seed)
    array_x, array_y, array_z = load_station_layout(arguments.array_coordinates,
                            sample_size = arguments.n_antennas)

    observation_duration_in_seconds = int(arguments.observation_duration *
                                          __MINUTE_IN_SECONDS)
    ra, dec = coords_into_radians(arguments.source_coordinates)

    uvw = generate_uvw_per_observation(array_x, array_y, array_z, ra, dec,
                                       start_time,
                                       observation_duration_in_seconds)
    uvw = select_baseline_length(uvw, arguments.max_baseline_length)
    logger.debug("UVW generated")
    rotated_uvw, pca, r_pca = rotate_ellipse_points(uvw)
    ecc, filling_factor, sampling, mean_coverage, mean_d = compute_parameters(rotated_uvw)

    if(arguments.make_uvw_plot):
        make_plots(uvw, rotated_uvw, sampling, arguments.frequency, pca, r_pca, arguments.save_image_to)

    data = dict(frequency=arguments.frequency, duration=observation_duration_in_seconds,
                ra=ra, dec=dec, out_dir=arguments.save_image_to, e=ecc,
                filling_factor=filling_factor, average_coverage=mean_coverage,
                average_scale=mean_d)
    with open(arguments.save_image_to + '/parameters.json', 'w') as f_out:
        json.dump(data, f_out)


if __name__== '__main__':
    main()
