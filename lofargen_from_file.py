import datetime
import os
from argparse import ArgumentParser
import json
from lofardatagenerator.coords import *
import logging
import numpy as np
logging.basicConfig(format='%(asctime)-15s %(levelname)s %(message)s')
logger = logging.getLogger('LOFAR generator')
logger.setLevel(logging.DEBUG)

__MINUTE_IN_SECONDS = 60


def parse_arguments():
    parser = ArgumentParser('Generate uvw and compute eccentricity and '
                            'filling factor of the uvw plane')


    parser.add_argument('station_field',
                        type=str,
                        help='coordinate list of the array elements')

    parser.add_argument('observation_table',
                        type=str,
                        help='table with the observation information')

    parser.add_argument('--make_uvw_plot', action='store_true')
    parser.add_argument('--print_n_rows', action='store_true', help='print nrows and exits')
    parser.add_argument('--start_row', type=int, default=0)
    parser.add_argument('--end_row', type=int, default=-1)

    parser.add_argument('--save_image_to', type=str, default='.')
    parser.add_argument('--full_table', type=str, default='summary.csv')

    return parser.parse_args()


import pandas as pd


def load_station_type_map(filename):
    station_map = pd.read_csv(filename, sep='\t', index_col=['NAME', 'TYPE'])
    return station_map


def load_observation_table(filename) -> pd.DataFrame:
    sas_id = pd.read_csv(filename, sep='\t', index_col=['SASID'], parse_dates=['STARTTIME', 'ENDTIME'])
    return sas_id


filter_to_type = {
    '10-90 MHz': 'LBA',
    '30-90 MHz': 'LBA',
    '110-190 MHz': 'HBA'
}

filter_to_frequency = {
    '10-90 MHz': 50,
    '30-90 MHz': 60,
    '110-190 MHz': 150
}


def station_list_to_array(station_array, station_list, filter):
    array_x = []
    array_y = []
    array_z = []
    for station in station_list:
        type = filter_to_type[filter]
        x = station_array.loc[station, type]['X'][0]
        y = station_array.loc[station, type]['Y'][0]
        z = station_array.loc[station, type]['Z'][0]

        array_x.append(x)
        array_y.append(y)
        array_z.append(z)
    return array_x, array_y, array_z

import gc
def main():
    arguments = parse_arguments()
    station_map = load_station_type_map(arguments.station_field)
    observation_table = load_observation_table(arguments.observation_table)
    summary_table = pd. \
        DataFrame(columns=['sas_id', 'beam', 'start_time', 'end_time',
                           'duration', 'ra', 'dec', 'e', 'filling_factor', 'average_coverage',
                           'average_scale'])
    nrows = len(observation_table.index)
    if (arguments.print_n_rows):
        print('number of rows are ', nrows)
        raise SystemExit()
    start_index = arguments.start_row
    end_index = arguments.end_row if arguments.end_row > 0 else nrows
    for index in range(start_index, end_index):
        row = observation_table.iloc[index]
        logger.info('processing observation %s/%s', index, nrows)
        sas_id = observation_table.index[index]
        ra, dec = row['RIGHTASCENSION'], row['DECLINATION']
        ra, dec = ra_dec_into_radians(ra, dec)
        station_list = row['STATIONS'].split(',')
        start_time: datetime.datetime = row['STARTTIME']
        end_time: datetime.datetime = row['ENDTIME']

        observation_duration_in_seconds = (end_time - start_time).seconds

        if not observation_duration_in_seconds:
            continue
        filter = row['FILTER']
        array_x, array_y, array_z = station_list_to_array(station_map, station_list, filter)
        beam = row['BEAMNUMBER']
        logger.info('generating UVW for sas_id %s and beam %s', index, beam)
        uvw = generate_uvw_per_observation(array_x, array_y, array_z, ra, dec,
                                           start_time,
                                           observation_duration_in_seconds)
        rotated_uvw, pca, r_pca = rotate_ellipse_points(uvw)
        ecc, filling_factor, sampling, mean_coverage, mean_d = compute_parameters(rotated_uvw)
        path = f'{arguments.save_image_to}/{sas_id}_{beam}/'
        os.makedirs(path, exist_ok=True)
        if (arguments.make_uvw_plot):
            make_plots(uvw, rotated_uvw, sampling, filter_to_frequency[filter], pca, r_pca, path)

        data = dict(frequency=filter_to_frequency[filter],
                    start_time=start_time.isoformat(),
                    end_time=end_time.isoformat(),
                    duration=observation_duration_in_seconds,
                    ra=ra, dec=dec, e=ecc,
                    sas_id=int(sas_id), beam=int(beam),
                    filling_factor=filling_factor, average_coverage=mean_coverage,
                    average_scale=mean_d)

        summary_table = summary_table.append(data, ignore_index=True)
        summary_table['sas_id'] = summary_table['sas_id'].astype(int)
        summary_table['beam'] = summary_table['beam'].astype(int)
        summary_table.to_csv(arguments.full_table, index=None)

        with open(path + '/parameters.json', 'w') as f_out:
            json.dump(data, f_out)

        gc.collect()

if __name__ == '__main__':
    main()
