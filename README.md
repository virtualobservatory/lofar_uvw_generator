# LOFAR UVW Generator

The software is able to generate UVW coverage given a series of stations
names or antenna fields.
Furthermore, the repository contains additional tools that can be used to source 
the UVW coverage data generation on some pre existent observation.

The code is also able to compute UV coverage summary parameter such as the
UV filling factor or the UV eccentricity. 
Please refer to [ObsCoreExtensionForVisibilities](https://github.com/ivoa/ObsCoreExtensionForVisibilityData.git) 
for more information regarding such parameters.


## Scrips available in this repository

### lofargen.py 

Generates random UVW coverage choosing randomly given a set of parameters.
Such parameters are:

- n_antennas
- source_coordinates
- frequency
- max_baseline_length
- observation_duration
- array_coordinates

The last two parameters are mandatory and represent the list of the the antenna coordinates
and the duration of the observation. 

If additionally to the summary table also summary uvw plots are requested the generation can be toggled with
`--make_uvw_plot`.

Here after the usage:
```bash
usage: Generate uvw and compute eccentricity and filling factor of the uvw plane
       [-h] [--n_antennas N_ANTENNAS]
       [--source_coordinates SOURCE_COORDINATES] [--make_uvw_plot]
       [--random_seed RANDOM_SEED] [--frequency FREQUENCY]
       [--max_baseline_length MAX_BASELINE_LENGTH]
       [--save_image_to SAVE_IMAGE_TO]
       array_coordinates observation_duration

positional arguments:
  array_coordinates     coordinate list of the array elements
  observation_duration  duration of the observation in minutes

optional arguments:
  -h, --help            show this help message and exit
  --n_antennas N_ANTENNAS
                        antenna sample size
  --source_coordinates SOURCE_COORDINATES
                        source_coordinates
  --make_uvw_plot
  --random_seed RANDOM_SEED
                        seed for the random number generator
  --frequency FREQUENCY
                        frequency in MHz
  --max_baseline_length MAX_BASELINE_LENGTH
                        max baseline length in m
  --save_image_to SAVE_IMAGE_TO
```

### lofargen_from_file.py

Generates the UVW coverage given an input file containing the following columns:

|Column name| Description|
|-----------|------------|
|SASID      |            |
|STARTTIME ||
| ENDTIME ||
|BEAMNUMBER ||
|FILTER            _LOFAR filter_|
|ANTENNASET      | _LOFAR antennaset (e.g. HBA_Dual)_ |
|RIGHTASCENSION ||
|DECLINATION     ||
|STATIONS        | _comma separeted list of stations_|

The parameters needed for the execution of the script are the following:
```bash

usage: Generate uvw and compute eccentricity and filling factor of the uvw plane
       [-h] [--make_uvw_plot] [--print_n_rows] [--start_row START_ROW]
       [--end_row END_ROW] [--save_image_to SAVE_IMAGE_TO]
       [--full_table FULL_TABLE]
       station_field observation_table

positional arguments:
  station_field         coordinate list of the array elements
  observation_table     table with the observation information

optional arguments:
  -h, --help            show this help message and exit
  --make_uvw_plot
  --print_n_rows        print nrows and exits
  --start_row START_ROW
  --end_row END_ROW
  --save_image_to SAVE_IMAGE_TO
  --full_table FULL_TABLE where to store the full table containing all the 
                          parameters

```


### lofarquery_lta.py

Queries the LOFAR LTA to produce the file that serves as a source for the 
`lofargen_from_file.py` script.

To use it the LTA credentials are required and can be specified via 
a configuration file who has to have the following structure:
```ini
[global]
database_user        : [username]
database_password    : [password]
```

The script has the following parameters:
```bash
usage: lofarquery_lta.py [-h] [--config CONFIG] [--verbose]
                         [--chunk_size CHUNK_SIZE]
                         outfile

Query the LTA to obtain the dataproduct list

positional arguments:
  outfile               where to store the queried table

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG       configuration file where the credentials are stored
  --verbose             set verbose logging level
  --chunk_size CHUNK_SIZE
                        save data in the csv in chunks of rows
```

