from datetime import datetime, timedelta
import sys
from astropy.coordinates import SkyCoord
import pyuvwsim
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import scipy
import numpy
import logging
from astropy.coordinates import AltAz, EarthLocation
import astropy.units as u
__DEFAULT_LOFAR_ARRAY_CENTER = EarthLocation.from_geocentric(*[3183230.104, 1276816.818, 5359477.705], unit=u.m)


def local_coords_to_radians(alt, az, location=__DEFAULT_LOFAR_ARRAY_CENTER):
    local_frame = AltAz(location=location, obstime=datetime.now())
    local_coord = SkyCoord(alt=alt * u.deg, az=az * u.deg, frame=local_frame)
    sky_coord = local_coord.transform_to('icrs')
    return sky_coord.ra.value, sky_coord.dec.value


def coords_into_radians(coords_string):
    c = SkyCoord(coords_string, unit=(u.hourangle, u.deg))
    ra, dec = c.ra.radian, c.dec.radian
    return ra, dec

def ra_dec_into_radians(ra, dec):
    c = SkyCoord(ra=ra, dec=dec, unit=(u.deg, u.deg))
    ra, dec = c.ra.radian, c.dec.radian
    return ra, dec


def load_station_layout(file_path: str, sample_size=10, max_baseline_length=-1):

    coords = numpy.loadtxt(file_path, delimiter=',')
    max_sample = coords.shape[0]
    if sample_size > 0 and sample_size <= max_sample:
        print(max_sample, numpy.arange(0, max_sample, 1))
        sample = numpy.arange(0, max_sample, 1)
        numpy.random.shuffle(sample)
        sample = sample[:sample_size]
        return coords[sample, 0], coords[sample, 1], coords[sample, 2]
    else:
        return coords[:, 0], coords[:, 1], coords[:, 2]


def get_mjd_from_datetime(time : datetime):
    mjd = pyuvwsim.datetime_to_mjd(time.year,
                                    time.month,
                                    time.day,
                                    time.hour,
                                    time.minute,
                                    time.second)
    return mjd


def generate_uvw_per_observation(array_x, array_y, array_z, ra, dec, start_time,
                                 observation_duration):

    n_antennas = len(array_x)
    n_baselines = int(n_antennas * (n_antennas - 1) // 2)

    uvw = numpy.zeros((observation_duration, n_baselines, 3))

    for i in range(observation_duration):
        mjd = get_mjd_from_datetime(start_time + timedelta(seconds = i))
        u, v, w = pyuvwsim.evaluate_baseline_uvw(array_x,
                                                 array_y,
                                                 array_z,
                                                 ra, dec, mjd)
        uvw[i, :, 0] = u
        uvw[i, :, 1] = v
        uvw[i, :, 2] = w
    u, v, w = uvw[:, :, 0].flatten(), \
              uvw[:, :, 1].flatten(), \
              uvw[:, :, 2].flatten()
    uvw_size = len(u)

    points_array = numpy.zeros((2 * uvw_size, 3), dtype=numpy.float32)
    points_array[:uvw_size, 0] = u
    points_array[:uvw_size, 1] = v
    points_array[:uvw_size, 2] = w

    points_array[uvw_size:, 0] = -u
    points_array[uvw_size:, 1] = -v
    points_array[uvw_size:, 2] = -w

    return points_array


def rotate_ellipse_points(uvw):
    __NDIMS = len(uvw.shape)

    pca = PCA(n_components=3)

    mean_uvw = numpy.median(uvw, axis=0)
    mean_uvw = uvw - mean_uvw
    principal_components = pca.fit_transform(mean_uvw.T)

    principal_component_0 = principal_components[0]
    principal_component_1 = principal_components[1]

    rot_axis = [1., 0, 0]
    cos_teta = numpy.dot(principal_component_0, rot_axis) / \
               numpy.linalg.norm(principal_component_0) / \
               numpy.linalg.norm(rot_axis)

    sin_teta = numpy.sqrt(1 - cos_teta**2)

    rotation_matrix = numpy.zeros((3,3))
    rotation_matrix[0, 0] = cos_teta
    rotation_matrix[0, 1] = sin_teta
    rotation_matrix[1, 0] = -sin_teta
    rotation_matrix[1, 1] = cos_teta
    rotation_matrix[2, 2] = 1

    rotated_matrix = numpy.dot(rotation_matrix.T, mean_uvw.T)
    rotated_components = numpy.dot(rotation_matrix.T, numpy.array(principal_components).T)

    return rotated_matrix, principal_components, rotated_components


def compute_parameters(rotated_matrix):
    u = rotated_matrix[0, :]
    v = rotated_matrix[1, :]
    baseline = numpy.sqrt(u ** 2 + v ** 2)
    grid_step = numpy.max(baseline) / 100

    max_u, min_u = max(rotated_matrix[0, :]), min(rotated_matrix[0, :])
    max_v, min_v = max(rotated_matrix[1, :]), min(rotated_matrix[1, :])
    max_w, min_w = max(rotated_matrix[2, :]), min(rotated_matrix[2, :])

    sampling_u = numpy.floor((max_u - min_u) / grid_step)
    sampling_v = numpy.floor((max_v - min_v) / grid_step)
    sampling = int(max([sampling_u, sampling_v]))

    dv = max_v - min_v
    du = max_u - min_u
    ecc = min([du, dv]) / max([du, dv])

    hist, ax1, ax2 = numpy.histogram2d(rotated_matrix[0, :],
                                       rotated_matrix[1, :],
                                       bins=sampling)
    d = numpy.sqrt(ax1[:-1, numpy.newaxis] ** 2 + ax2[numpy.newaxis, :-1] ** 2)

    mean_coverage = numpy.mean(hist)

    mean_d = numpy.average(d, weights=hist)
    filling_factor = len(hist[hist > 0]) / (hist.shape[0] * hist.shape[1])
    return ecc, filling_factor, sampling, mean_coverage, mean_d


def plot_lm_coverage(uvw, sampling, frequency_in_mhz):
    #TODO: THIS might be incorrect. Check
    __MHZ_IN_HZ = 1.e6
    __DEG_IN_ARCSEC = 206264.80624709636
    hist, ax1, ax2 = numpy.histogram2d(uvw[:, 0], uvw[:, 1], bins=sampling)
    ubl = ax1[1] - ax1[0]
    vbl = ax2[1] - ax2[0]

    freq = frequency_in_mhz * __MHZ_IN_HZ

    xcoeff = 1./(numpy.pi * ubl / scipy.constants.c * freq)
    ycoeff = 1./(numpy.pi * vbl / scipy.constants.c * freq)

    values = numpy.zeros_like(hist)
    values[hist > 0] = 1

    plt.imshow(abs(numpy.fft.fftshift(numpy.fft.fft2(values))),
               extent=[-xcoeff, xcoeff,
                       -ycoeff, ycoeff])
    plt.xlabel('m')
    plt.ylabel('l')


def make_uv_hist_plot(uvw, sampling, pca):
    hist, ax1, ax2 = numpy.histogram2d(uvw[:, 0], uvw[:, 1], bins=sampling)
    plt.minorticks_on()
    plt.imshow(numpy.log10(hist), extent=[
        ax2[0] * 1.e-6, ax2[-1] * 1.e-6,
        ax1[0] * 1.e-6, ax1[-1] * 1.e-6
    ], aspect='equal')

    plt.quiver(0, 0, pca[0][0], pca[0][1], scale=5)
    plt.quiver(0, 0, pca[1][0], pca[1][1], scale=5)
    plt.xlabel('x [Mm]')
    plt.ylabel('y [Mm]')

    plt.colorbar()

def make_baseline_hist_plot(uvw, sampling):
    u = uvw[:, 0]
    v = uvw[:, 1]
    w = uvw[:, 2]
    baselines = numpy.sqrt(u ** 2 + v ** 2 + w ** 2) / 1.e6

    plt.hist(baselines, bins=sampling)
    plt.semilogy()
    plt.ylabel('N samples')
    plt.xlabel('$\sqrt{u^2 + v^2 + w^z}$ [Mm]')


def make_plots(original_uvw, rotated_uvw, sampling, frequency_in_mhz, pca, r_pca, path):
    plt.figure('original_uvw')
    make_uv_hist_plot(original_uvw, sampling, pca)

    plt.savefig(path + '/original_uvw.png')
    plt.close()

    plt.figure('lm coverage')
    plot_lm_coverage(original_uvw, sampling, frequency_in_mhz)
    plt.savefig(path + '/lm_coverage.png')
    plt.close()

    plt.figure('baselines histogram')
    make_baseline_hist_plot(original_uvw, sampling)
    plt.savefig(path + '/baseline_histogram.png')
    plt.close()

    plt.figure('rotated_uvw')
    make_uv_hist_plot(rotated_uvw.T, sampling, r_pca)
    plt.savefig(path + '/rotated_uvw.png')
    plt.close()


def select_baseline_length(uvw, max_baseline_length):
    if max_baseline_length > 0:
        x = uvw[:, 0]
        y = uvw[:, 1]
        z = uvw[:, 2]
        dist = numpy.sqrt(x**2. + y**2. + z**2.)
        print(dist)
        uvw_new = uvw[dist <= max_baseline_length, :]
        if len(uvw_new) == 0:
            logging.error('max length %s selects an empty sample of antennas',
                         max_baseline_length)
            sys.exit(1)

        return uvw_new
    else:
        return uvw
