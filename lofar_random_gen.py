import numpy
from argparse import ArgumentParser
import json
import logging
from lofardatagenerator.coords import *

logger = logging.getLogger('LOFAR generator')
logger.setLevel(logging.DEBUG)

__MINUTE_IN_SECONDS = 60

def parse_arguments():
    parser = ArgumentParser('Generate uvw and compute eccentricity and '
                            'filling factor of the uvw plane')
    parser.add_argument('array_coordinates',
                        type=str,
                        help='coordinate list of the array elements')

    parser.add_argument('observation_duration',
                        type=int,
                        nargs='+',
                        help='duration of the observation in minutes')
    parser.add_argument('--n_antennas',
                        type=int,
                        default=-1,
                        help='antenna sample size')

    parser.add_argument('--min_alt',
                        type=float,
                        default='10',
                        help='source_coordinates')
    parser.add_argument('--n_pointings',
                        type=float,
                        default=100)

    parser.add_argument('--random_seed',
                        default=numpy.random.seed(),
                        type=int,
                        help='seed for the random number generator')

    parser.add_argument('--frequency',
                        default=150.,
                        type=float,
                        help='frequency in MHz')

    parser.add_argument('--max_baseline_length',
                        default=-1,
                        type=float,
                        help='max baseline length in m')
    parser.add_argument('--save_image_to', type=str, default='out')
    return parser.parse_args()


import os
def main():
    start_time = datetime.now()
    arguments = parse_arguments()
    numpy.random.seed(arguments.random_seed)
    for i in range(arguments.n_pointings):

        array_x, array_y, array_z = load_station_layout(arguments.array_coordinates,
                            sample_size = arguments.n_antennas)

        observation_duration_in_seconds = int(numpy.random.choice(arguments.observation_duration) *
                                              __MINUTE_IN_SECONDS)
        alt = numpy.random.uniform([arguments.min_alt, 90], 1)[0]
        az = numpy.random.uniform([0, 360], 1)[0]

        ra, dec = local_coords_to_radians(alt, az)

        uvw = generate_uvw_per_observation(array_x, array_y, array_z, ra, dec,
                                           start_time,
                                           observation_duration_in_seconds)
        uvw = select_baseline_length(uvw, arguments.max_baseline_length)
        logger.debug("UVW generated")
        rotated_uvw, pca, r_pca = rotate_ellipse_points(uvw)
        ecc, filling_factor, sampling, mean_coverage, mean_d = compute_parameters(rotated_uvw)
        out_path = os.path.join(arguments.save_image_to, f'POINTING_{i}')
        os.makedirs(out_path, exist_ok=True)
        make_plots(uvw, rotated_uvw, sampling, arguments.frequency, pca, r_pca, out_path)

        data = dict(
                    duration=observation_duration_in_seconds,
                    ra=ra, dec=dec, out_dir=arguments.save_image_to, e=ecc,
                    filling_factor=filling_factor, average_coverage=mean_coverage,
                    average_scale=mean_d)
        with open(os.path.join(arguments.save_image_to, f'parameters_pointing_{i}.json'), 'w') as f_out:
            json.dump(data, f_out)


if __name__== '__main__':
    main()
